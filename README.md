# Spell checker performance comparator

[Scroll down to English](#presentation)

## Présentation

Ce travail a été réalisé dans le cadre de la thèse de Johana Bodard au sein du [laboratoire CHArt](http://laboratoire-chart.fr/) de l'[université Paris 8](https://www.univ-paris8.fr/).

L'objectif de ce travail est d'évaluer et de comparer les performances de correcteurs orthographiques sur un corpus de textes rédigés par des personnes dyslexiques françaises.
Ce corpus, appelé *Corpus DYS*, est disponible sur [Nakala](https://nakala.fr/10.34847/nkl.ced0370u).

Ce travail peut également être utilisé pour évaluer les performances de correcteurs orthographiques sur d'autres corpus.

## Contenu

- *spellchecker_performance_comparator.ipynb* : un notebook Jupyter Python qui permet d'évaluer et de comparer les performances de correcteurs orthographiques ; 
- *getting_started_fr.pdf* : un document expliquant comment installer un environnement de développement pour exécuter le notebook ;
- *README.md* : le présent fichier.

## Presentation

This work was carried out as part of Johana Bodard's PhD thesis in the [CHArt laboratory](http://laboratoire-chart.fr/) at the [Paris 8 University](https://www.univ-paris8.fr/).

The objective of this work is to evaluate and compare the performance of spell checkers on a corpus of texts written by by French people with dyslexia.
This corpus, nammed *Corpus DYS*, is available on [Nakala](https://nakala.fr/10.34847/nkl.ced0370u).

This work can also be used to evaluate the performance of spell checkers on other corpora.

## Content

- *spellchecker_performance_comparator.ipynb*: a Jupyter Python notebook for evaluating and comparing the performance of spell checkers; 
- *getting_started_fr.pdf*: a guide explaining how to set up a development environment to run the notebook;
- *README.md*: the current file.